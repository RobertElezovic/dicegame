var timesPlayed = 0;
var totalScore = 0;

function rollDice(){
if (document.getElementById("GuessUser").value >= 3 && document.getElementById("GuessUser").value <= 18) {
	timesPlayed += 1;
    var d1 = Math.floor(Math.random() * 6) + 1;
    var d2 = Math.floor(Math.random() * 6) + 1;
	var d3 = Math.floor(Math.random() * 6) + 1;
    var diceTotal = (d1 + d2 + d3);

 var playerGuess = document.getElementById("GuessUser").value;
 if (playerGuess > diceTotal && playerGuess <= 18) {
    alert("Guess higher than sum, You got no score. \nDices sum: " + diceTotal + "\nYour  total score: " + totalScore + "\nTimes played: " + timesPlayed);
  }
   else if (playerGuess <= diceTotal) {
    var dice4 = Math.floor(Math.random() * 6 + 1);
    totalScore += dice4 * playerGuess;
    var score = dice4 * playerGuess;
    if (playerGuess == diceTotal && playerGuess <= 18) {
      alert ("Your guess was the same as the dice-sum. Your guess is multiplied by the fourth dice.\n\nThe fourth dice: " + dice4 +  "\nGuess * 4th dice ="+score+" \nSum of 3 first dices: " + diceTotal + "\nYour total score: " + totalScore + "\nTimes played: " + timesPlayed);
    }
	 else if (playerGuess < diceTotal && playerGuess <= 18) {
      alert ("Your guess was lower than the dice-sum. Your guess is multiplied by the fourth dice.\n\nThe 4th dice: " + dice4 +  "\nGuess * 4th dice = "+score+" \n3 Dices sum: " + diceTotal + "\nYour  total score: " + totalScore + "\nTimes played: " + timesPlayed);
    }
  }
  if (timesPlayed >= 10) {
	alert("Your score is: " + totalScore + "\nPress OK to play again, otherwise click the dice game logo to return to the main menu");
    timesPlayed = 0;
    SendScore(totalScore);
    totalScore = 0;
	}
  }
  
  	else {
		alert("Please enter a number between 3 and 18")
	}
}