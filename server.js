var loginresponse

$(document).ready(function() {
	
	$("#btn-logout").hide()
	
	$("#btn-highscore").hide()
	
	$("#btn-createaccount2").click(function() {
		if (checkcreateinfo()) {
			var request = new XMLHttpRequest()
			request.open("POST", "http://193.10.30.163/users", true)
			request.setRequestHeader("Content-Type", "application/json")
			request.addEventListener("load", function() {
				if (this.readyState == 4) {
				console.log("We got a response from the server")
				console.log("Status", request.status)
				console.log("Body: ", request.responseText)
				if (this.status == 200) {
				alert("Your account has succesfully been created, login on the next page GLHF!")
				$("#btn-highscore").hide()
				$("#btn-logout").hide()
				$("#startMenu").fadeIn();
				$("#info").fadeIn();
				$("#loginform").hide();
				$("#gamediv").hide();
				$("#scorediv").hide();
				}
				else if (this.status ==422){
				answer = JSON.parse(this.responseText)
					if (answer.emailTaken == true && answer.usernameTaken ==true){
						alert("That e-mail and username is already taken")
					}
					else if(answer.emailTaken==true){
						alert("That e-mail is already taken")
					}
					
					else if (answer.usernameTaken == true) {
						alert("That username is already taken")
					}
				}
				
			}
		})
	}
			var info = {"email": "memberA@members.com", "username": "MemberA", "password": "memberA", "firstName": "Anna", "lastName": "Bas"}
			info.firstName = document.getElementById("Firstname").value
			info.lastName = document.getElementById("Lastname").value
			info.username = document.getElementById("Username").value
			info.email = document.getElementById("Email").value
			info.password = document.getElementById("Password").value
			console.log(info)
			request.send(JSON.stringify(info))
		
	})
	
	$("#btn-login").click(function() {
		var request = new XMLHttpRequest()
		request.open("POST", "http://193.10.30.163/users/login", true)
		request.setRequestHeader("Content-Type", "application/json")
		request.addEventListener("load", function() {
			console.log("We got a response from the server")
			console.log("Status", request.status)
			console.log("Body: ", request.responseText)
			loginresponse = JSON.parse(request.responseText)	
			if (request.status == 200) {
				$("#btn-logout").fadeIn()
				$("#btn-highscore").show()
				alert("You have signed in, you can now view all highscores \n(Use the DICE GAME logo to navigate to the homescreen)")
				response = JSON.parse(this.responseText)
				GetYourScores()
				GetHighScores()
			}
			 else if (this.status = 401) {
            answer = JSON.parse(this.responseText)
            if (answer.wrongEmail == true) {
              alert("Wrong e-mail, try again")
            }
            else if (answer.wrongPassword == true) {
              alert("Wrong password, try again is wrong")
            }
          }
		})
		var info = {"email": "memberA@members.com", "password": "memberA"}
		info.email = document.getElementById("Loginemail").value
		info.password = document.getElementById("Loginpassword").value
		console.log(info)
		request.send(JSON.stringify(info))
	})
	
	$("#btn-logout").click(function() {
		var request = new XMLHttpRequest()
		request.open("POST", "http://193.10.30.163/users/logout", true)
		request.setRequestHeader("Content-Type", "application/xml")
		request.addEventListener("load", function() {
			console.log("We got a response from the server")
			console.log("Status", request.status)
			console.log("Body: ", request.responseText)
			if (request.status == 200) {
				alert("Thank you for playing this game")
				$("#btn-highscore").hide()
				$("#btn-logout").hide()
				$("#startMenu").fadeIn();
				$("#info").fadeIn();
				$("#loginform").hide();
				$("#gamediv").hide();
				$("#scorediv").hide();
			}
		})
		request.send('<data> <session>' + loginresponse.session + '</session> </data>')
	})

	
})

function GetYourScoresCallback (response) {
	console.log("We got a response from the server")
	console.log("Status", response.status)
	console.log("Body: ", response.data)
	
	response.data.scores.sort(function(a, b) {return b.score - a.score})
	var userscoresTable = document.getElementById("UserScoresTable");
	var tablelength = userscoresTable.rows.length;
	for (var i = 0; i < tablelength; i++) {
		userscoresTable.deleteRow(0)
	}
	for (var i = 0; i < response.data.scores.length && i < 10; i++) {
		var row = userscoresTable.insertRow(i);
		var cell = row.insertCell(0);
		cell.innerHTML = response.data.scores[i].score;
	}
}

function GetHighScoresCallback(response) {
		console.log("We got a response from the server")
	console.log("Status", response.status)
	console.log("Body: ", response.data)
	
	response.data.scores.sort(function(a, b) {return b.score - a.score})
	var userscoresTable = document.getElementById("HighScoresTable");
	var tablelength = userscoresTable.rows.length;
	for (var i = 0; i < tablelength; i++) {
		userscoresTable.deleteRow(0)
	}
	for (var i = 0; i < response.data.scores.length && i < 10; i++) {
		var row = userscoresTable.insertRow(i);
		var usercell = row.insertCell(0);
		var scorecell = row.insertCell(1);
		usercell.innerHTML = response.data.scores[i].username;
		scorecell.innerHTML = response.data.scores[i].score;
	}
}

function GetYourScores () {
	var script = document.createElement("script")
	script.src = "http://193.10.30.163/scores/"+loginresponse.username+"?callback=GetYourScoresCallback&session="+loginresponse.session
	document.head.appendChild(script)
}

function GetHighScores() {
	var script = document.createElement("script")
	script.src = "http://193.10.30.163/scores?callback=GetHighScoresCallback&session="+loginresponse.session
	document.head.appendChild(script)
}

function SendScore(totalscore) {
	
	var request = new XMLHttpRequest()
	request.open("POST", "http://193.10.30.163/scores/"+loginresponse.username)
	request.setRequestHeader("Content-Type", "application/xml")
	request.addEventListener("load", function() {
			console.log("We got a response from the server")
			console.log("Status", request.status)
			console.log("Body: ", request.responseText)	
			GetHighScores()
			GetYourScores()
	})
	request.send('<data> <session>' + loginresponse.session + '</session> <score>' + totalscore + '</score>  </data>')
}